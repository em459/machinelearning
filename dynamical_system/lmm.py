import sys
import numpy as np

'''Abstract base class for Linear Multistep Method (LMM)

1/h*\sum_{j=0}^q alpha_j*U_{n+j} = \sum_{j=0}^q beta_j*f_{n+j}

with f_{k} = f(U_k)

'''
class LMM(object):
    def __init__(self,func,h,U0=None):
        '''Create a new instance
        
        :arg func: function to integrate
        :arg h: time step size
        '''
        self._func = lambda x: np.array(func(x))
        self._h = h
        # Order of method
        self._q = 0
        # alphaa-array
        self._alpha = ()
        # beta-array
        self._beta = ()
        # U-array, stores U_{n-q},...,U_{n-1}
        self._U = []
        # fU-array, stores f_{n-q},...,f_{n-1}
        self._fU = []
        if not (U0 is None):
            self.initialise(U0)

    def _RK4step(self,y0):
        '''Carry out RK4 step. Given y_0 return y_1

        :arg y0: Current state y_0
        '''
        k1 = self._h*self._func(y0)
        k2 = self._h*self._func(y0+0.5*k1)
        k3 = self._h*self._func(y0+0.5*k2)
        k4 = self._h*self._func(y0+k3)
        return y0 + 1./6.*(k1+2.*k2+2.*k3+k4)

    def initialise(self,U0):
        '''Reset method and set initial value U0

        :arg U0: Starting value U0
        '''
        # Empty the queues
        self._U = [np.array(U0),]
        self._fU = [self._func(U0),]
    
    def step(self):
        '''Carry out one step and return new value U_n'''
        if (len(self._U)<self._q):
            Uold = self._U[-1]
            Unew = self._RK4step(Uold)
            self._U.append(Unew)
            self._fU.append(self._func(Unew))
        else:
            s = 0.0
            for j in range(self._q):
                s += self._h*self._beta[j]*self._fU[j] - self._alpha[j]*self._U[j]
            Unew = s/self._alpha[self._q]
            for j in range(self._q-1):
                self._U[j] = self._U[j+1]
                self._fU[j] = self._fU[j+1]
            self._U[self._q-1] = Unew
            self._fU[self._q-1] = self._func(Unew)
        return Unew
    
'''Class for Adams Bashforth LMMs up to order 3'''
class AdamsBashforth(LMM):
    def __init__(self,func,h,q,U0=None):
        super(AdamsBashforth,self).__init__(func,h,U0)
        self._q = q
        if (q==1):
            self._alpha = (-1.0, 1.0)
            self._beta = (1.0,)
        elif (q==2):
            self._alpha = (0.0, -1.0, 1.0)
            self._beta = (-1.0/2.0, 3.0/2.0)
        elif (q==3):
            self._alpha = (0.0, 0.0, -1.0, 1.0)
            self._beta = (5.0/12.0, -4.0/3.0, 23.0/12.0)
        else:
            print('Order of AdamsBashforth method has to be between 1 and 3')
            sys.exit(0)

'''Class for unstable method from example 1.4.3 in the lectures'''
class Unstable(LMM):
    def __init__(self,func,h,U0=None):
        super(Unstable,self).__init__(func,h,U0)
        self._q = 2
        self._alpha = (-5.0/6.0, 4.0/6.0, 1.0/6.0)
        self._beta = (1.0/3.0, 2.0/3.0)
