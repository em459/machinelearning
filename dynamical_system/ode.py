from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import tensorflow as tf
from lmm import *
from plotting import *
from networks import *

'''*** Solve ODE with multi-layer neural network ***

Solve a d-dimensional ODE of the form

dy/dt = f(y(t))

y(t=0) = y_0

where y=y(t) is the solution in d dimensions 

Structure of the network is:

z_1 = sigmoid(t*W_1 + y^{prior}(t).W_prior + b_1)
z_2 = sigmoid(z_1.W_2 + b_2)
z_3 = sigmoid(z_2.W_3 + b_3)
y' = z_3.Wtilde + btilde

We assume that the initial condition is y(0)_i = 1 for all i.
To strictly enforce this, consider the function

y_i = y'_i/y'_i(t=0) instead.

The ODE considered here is the an-harmonic oscillator, 

dy_0/dt = y_1, dy_1/dt = -y_0 - alpha*y_0^3

The prior which is fed into the nextwork is the exact solution
of the equation for alpha=0, i.e.

y^{prior}_0(t) =  cos(t) + sin(t)
y^{prior}_1(t) = -sin(t) + cos(t) 
'''

# Seed random number generators to ensure reproducibility of results
np.random.seed(74911917)
tf.set_random_seed(12418815)


# Initial condition
U0 = (1.0, 1.0)

def y_exact(h,T_final):
    '''Calculate the exact solution at the final time. A third order
    Adams-Bashforth integrator with time step h is used.

    :arg h: Time step size to be used in Adams-Bashforth method
    :arg T_final: Final time
    '''
    # Degree 3 Adams-Bashforth integrator for calculating the 'exact' solution
    integrator = AdamsBashforth(lambda U: (U0[1],-U0[0]-alpha*U0[0]**3),
                                h,
                                q=3,
                                U0=U0)

    t_traj = 0.0
    while (t_traj + 0.5*h < T_final):
        y_ex = integrator.step()
        t_traj += h
    return y_ex
                
# Placeholder for time
t = tf.placeholder(dtype=tf.float32,shape=(None,1),name='t')

# Number of nodes in internal layers
nhiddennodes = 64

# Number of hidden internal layers
nlayer = 1

network = DenseNetwork(nhiddennodes,nlayer)

# Solution
y = tf.divide(network.build(t),network.build(tf.constant([[0.,]])))
# Rate of change dy(t)/dt
dy_dt = tf.concat([tf.gradients(y_,t)[0] for y_ in tf.split(y,2,axis=1)],1)

# Forcing function f(y(t))
alpha = 0.5
y0, y1 = tf.split(y,2,axis=1)
f = tf.concat([y1,-y0-alpha*tf.pow(y0,3)],1)
# Energy 1/2*(dy/dt)^2 + 1/2*y^2 + 1/4*alpha*y^4
energy = 0.5*tf.square(y0)+0.5*tf.square(y1)+0.25*alpha*tf.pow(y0,4)
energy_0 = 0.5*U0[0]**2+0.5*U0[1]**2+0.25*alpha*U0[0]**4

# Energy deviation = difference between current and initial energy
energy_deviation = tf.reduce_mean(tf.square(tf.subtract(energy,tf.constant(energy_0))))

# Loss function. We are trying to minimise
# dy/dt - f (which is zero for the exact solution) and the deviation of the
# energy (which should remain constant) from the initial value 
loss = tf.reduce_mean(tf.square(dy_dt-f))+energy_deviation

# Loss function for each timestep
local_loss = tf.reduce_sum(tf.square(dy_dt-f),axis=1)

# Gradient descent optimiser
optimizer = tf.train.AdagradOptimizer(0.1)
train = optimizer.minimize(loss)

# Number of training epochs
training_epochs=256
# Size of batches
batch_size = 500
# Number of gradient descent steps per batch
descent_steps = 2000

# Final time
T_final = 2.*np.pi

# Time step size for outputting the solution
h = 0.1

# Time values at which the solution will be plotted in phase space
t_values = np.arange(0,1.5*T_final,h)

### Print parameters
print
print ('=== Parameters ===')
print ('nlayer                 = '+('%6d' % nlayer))
print ('nhiddennodes           = '+('%6d' % nhiddennodes))
print ('training_epochs        = '+('%6d' % training_epochs))
print ('batch_size             = '+('%6d' % batch_size))
print ('descent_steps          = '+('%6d' % descent_steps))
print ('T_final                = '+('%6.2f' % T_final)) 
print   
saver = tf.train.Saver()
checkpoint_filename='./ode.ckpt'
losses = []
errors = []
# Run session
with tf.Session() as sess:
    # Initialise global variables
    sess.run(tf.global_variables_initializer())
    # Check if checkpoint file exists, and load it if it does
    print ('*** Trying to restore model from file '+checkpoint_filename+' ***')
    try:
        saver.restore(sess, checkpoint_filename)
        print ('SUCCESS')
    except:
        print ('FAILED to load checkpoint file, re-initialising model')
    # Loop over all training epochs
    for epoch in range(training_epochs):
        batch_t = np.abs(np.random.normal(scale=2.*T_final,size=(batch_size,1)))
        for i in range(descent_steps):
            _, loss_value = sess.run((train,loss),feed_dict={t:batch_t})
        y_final = sess.run(y,feed_dict={t:np.asarray((T_final,)).reshape(1,1)})
        error = (np.linalg.norm(y_final-y_exact(h/16,T_final)))
        print ('epoch = '+('%6d' % epoch)+' loss = ',('%8.4e' % loss_value),' error = ',('%8.4e' % error))
        losses.append(loss_value)
        errors.append(error)
    # Save trained model in checkpoint file
    saver.save(sess, checkpoint_filename)
    trajectory = sess.run(y,feed_dict={t:t_values.reshape((len(t_values),1))})
    loss_function = sess.run(local_loss,feed_dict={t:t_values.reshape((len(t_values),1))})
    
plot_trajectory(t_values,trajectory,U0,alpha,T_final)
plot_history(losses,errors)
plot_loss_function(t_values,loss_function)

# Save Tensorflow graph to disk
writer = tf.summary.FileWriter('.')
writer.add_graph(tf.get_default_graph())
writer.flush()
