import numpy as np
import tensorflow as tf

class Network(object):
    ''' Base class for a neural network

    :arg m: Number of nodes in hidden layer
    :arg nlayer: Number of hidden layers
    '''
    def __init__(self,m,nlayer):
        self.m = m
        self.nlayer = nlayer

    def _get_variable(self,name,shape):
        '''Helper function for constructing a tensorflow variable

        :arg name: Name of variable
        :arg shape: Shape of variable
        '''
        eps_min = -1.0
        eps_max = +1.0
        return tf.get_variable(
            name,
            shape=shape,
            initializer=tf.random_uniform_initializer(
                minval=eps_min,
                maxval=eps_max))
    
    def build(self,t):
        pass
    
class DenseNetwork(Network):
    '''Dense network with nlayer hidden layers of m nodes each'''
    def __init__(self,m,nlayer):
        super(DenseNetwork,self).__init__(m,nlayer)
        # Dimension of problem
        d = 2
        # Internal variables which parametrise the network
        # Input mapping: t -> first hidden layer
        self.W_in = self._get_variable('W_in',[1,m])
        self.W_prior = self._get_variable('W_prior',[2,m])
        self.b_in = tf.get_variable('b_in',[1,m])
        # Hidden layers
        W_hidden = []
        b_hidden = []
        for i in range(nlayer):
            W_hidden.append(self._get_variable('W_hidden_'+str(i),[m,m]))
            b_hidden.append(self._get_variable('b_hidden_'+str(i),[1,m]))
        self.W_hidden = W_hidden
        self.b_hidden = b_hidden
        # sigmoid Output layer
        self.W_out = tf.get_variable('W_out',[m,d])
        self.b_out = tf.get_variable('b_out',[1,d])
        # Final linear-scaling layer
        self.W_linear = tf.get_variable('W_linear',[d,d])
        self.b_linear = tf.get_variable('b_linear',[d])
        
    def build(self,t):
        # Prior function
        y_prior = tf.concat([tf.cos(t)+tf.sin(t),-tf.sin(t)+tf.cos(t)],1)
        z = tf.sigmoid(tf.matmul(t,self.W_in)+self.b_in+tf.matmul(y_prior,self.W_prior))
        for ell in range(self.nlayer):
            z = tf.sigmoid(tf.matmul(z,self.W_hidden[ell])+self.b_hidden[ell])
        z = tf.sigmoid(tf.matmul(z,self.W_out)+self.b_out)
        # Output function y(t)
        return tf.matmul(z,self.W_linear)+self.b_linear

class LSTMNetwork(Network):
    '''Long short-term memory network'''
    def __init__(self,m,nlayer):
        super(LSTMNetwork,self).__init__(m,nlayer)
        d = 3
        D = 2
        # Input layer
        self.W_in = self._get_variable('W_in',[d,m])
        self.b_in = self._get_variable('b_in',[m])
        # Output layer
        self.W_out = self._get_variable('W_out',[m,D])
        self.b_out = self._get_variable('b_out',[D])
        # Hidden layers
        self.u_z_ell = []
        self.u_g_ell = []
        self.u_r_ell = []
        self.u_h_ell = []
        self.W_z_ell = []
        self.W_g_ell = []
        self.W_r_ell = []
        self.W_h_ell = []
        self.b_z_ell = []
        self.b_g_ell = []
        self.b_r_ell = []
        self.b_h_ell = []
        for i in range(self.nlayer):
            self.u_z_ell.append(self._get_variable('u_z_'+str(i),[d,m]))
            self.u_g_ell.append(self._get_variable('u_g_'+str(i),[d,m]))
            self.u_r_ell.append(self._get_variable('u_r_'+str(i),[d,m]))
            self.u_h_ell.append(self._get_variable('u_h_'+str(i),[d,m]))
            self.W_z_ell.append(self._get_variable('W_z_'+str(i),[m,m]))
            self.W_g_ell.append(self._get_variable('W_g_'+str(i),[m,m]))
            self.W_r_ell.append(self._get_variable('W_r_'+str(i),[m,m]))
            self.W_h_ell.append(self._get_variable('W_h_'+str(i),[m,m]))
            self.b_z_ell.append(self._get_variable('b_z_'+str(i),[m]))
            self.b_g_ell.append(self._get_variable('b_g_'+str(i),[m]))
            self.b_r_ell.append(self._get_variable('b_r_'+str(i),[m]))
            self.b_h_ell.append(self._get_variable('b_h_'+str(i),[m]))
        
    def build(self,t):
        x = tf.concat([t,
                       tf.cos(t)+tf.sin(t),
                       -tf.sin(t)+tf.cos(t)],1)
        S_ell = tf.sigmoid(tf.matmul(x,self.W_in)+self.b_in)
        for i in range(self.nlayer):
            Z_ell = tf.sigmoid(tf.matmul(x,self.u_z_ell[i])+tf.matmul(S_ell,self.W_z_ell[i])+self.b_z_ell[i])
            G_ell = tf.sigmoid(tf.matmul(x,self.u_g_ell[i])+tf.matmul(S_ell,self.W_g_ell[i])+self.b_g_ell[i])
            R_ell = tf.sigmoid(tf.matmul(x,self.u_r_ell[i])+tf.matmul(S_ell,self.W_r_ell[i])+self.b_r_ell[i])
            S_times_R_ell = tf.multiply(S_ell,R_ell)
            H_ell = tf.sigmoid(tf.matmul(x,self.u_h_ell[i])+tf.matmul(S_times_R_ell,self.W_h_ell[i])+self.b_h_ell[i])
            S_ell = tf.multiply(H_ell,1.-G_ell) + tf.multiply(Z_ell,S_ell)
        return tf.matmul(S_ell,self.W_out)+self.b_out
