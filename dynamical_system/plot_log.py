import numpy as np
from matplotlib import pyplot as plt
import re

import matplotlib as mpl

'''Plot data in log file

The entries of this file are assumed to be in the format

#        epoch =      2 loss =  1.1091e+00  error =  2.1948e+00

'''

mpl.rcParams['font.family'] = ['sans']
mpl.rcParams['font.size'] = 14

filename = 'log.txt'

regex = ' *epoch *= *([0-9]+) *loss *= *([0-9\.eE\+\-]+) *error *= *([0-9\.]+[eE][\+\-][0-9][0-9]) *'
epoch = []
loss = []
error = []
with open(filename) as f:
    for line in f.readlines():
        m = re.match(regex,line)
        if m:
            epoch.append(int(m.group(1)))
            loss.append(float(m.group(2)))
            error.append(float(m.group(3)))

epoch = np.asarray(epoch)
loss = np.asarray(loss)
error = np.asarray(error)

plt.clf()
plt.subplot(1,2,1)
ax = plt.gca()
plt.plot(epoch,loss,
         linewidth=2,
         color='blue')
ax.set_xlabel('epoch')
ax.set_ylabel('loss')
ax.set_yscale('log')
plt.xticks(rotation=30)
plt.subplot(1,2,2)
ax = plt.gca()
ax.yaxis.set_label_position('right')
plt.plot(epoch,error,
         linewidth=2,
         color='red')
ax.set_xlabel('epoch')
ax.set_ylabel('error')
ax.set_yscale('log')
plt.xticks(rotation=30)
plt.savefig('history.pdf',bbox_inches='tight')



