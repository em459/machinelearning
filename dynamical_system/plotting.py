'''Plotting routines for network'''

from lmm import *
from matplotlib import pyplot as plt
import numpy as np

def plot_trajectory(t_values,trajectory,U0,alpha,T_final):
    '''Plot the learned trajectory y(t) as a function of time
    
    This creates two plots:
    * a phase-space plot, with shows y_2(t) as a function of y_1(t)
    * a time series plot, which shows y_1(t) and y_2(t) as functions of time

    :arg t_values: Values of time points at which the trajectory is available
    :arg trajectory: List with phase space points (y_1(t),y_2(t)) for all 
      time steps
    '''
    # Time step size
    h = t_values[1]-t_values[0]
    # Degree 3 Adams-Bashforth integrator for calculating the 'exact' solution
    h_factor = 16 # Use a timestep size which is h_factor smaller than h
    integrator = AdamsBashforth(lambda U: (U[1],-U[0]-alpha*U[0]**3),
                                h/h_factor,
                                q=3,
                                U0=U0)

    # Construct the exact trajectory
    X_exact = [U0[0],]
    Y_exact = [U0[1],]
    t_traj = 0.0
    t_exact = [0.0,]
    while (t_traj + 0.5*h < 1.5*T_final):
        for i in range(h_factor):
            y_exact = integrator.step()
        t_traj += h
        t_exact.append(t_traj)
        X_exact.append(y_exact[0])
        Y_exact.append(y_exact[1])

    # Plot in phase space
    plt.clf()
    ax = plt.gca()
    ax.set_aspect('equal')
    ax.set_xlabel(r'$y(t)$')
    ax.set_ylabel(r'$\dot{y}(t)$')
    ax.set_xlim(-2.5,2.5)
    ax.set_ylim(-2,2)
    X = trajectory[:,0]
    Y = trajectory[:,1]
    # --- 1 --- Numerical solution from neural network
    plt.plot(X,Y,
             linewidth=2,
             color='blue',
             label='network',
             marker='o',
             markerfacecolor='blue',
             markeredgecolor='blue',
             markersize=4)
    # --- 2 --- Exact solution from Adams-Bashforth integrator
    plt.plot(X_exact,
             Y_exact,
             linewidth=2,
             color='red',
             marker='o',
             markersize=4,
             markerfacecolor='white',
             markeredgecolor='red',
             label='exact')
    # --- 3 --- Prior = solution for alpha=0
    plt.plot(np.cos(t_values)+np.sin(t_values),
             -np.sin(t_values)+np.cos(t_values),
             linewidth=2,
             color='green',
             marker='o',
             markersize=4,
             markerfacecolor='white',
             markeredgecolor='green',
             label='prior')
    plt.legend(loc='lower left')
    plt.savefig('trajectory_phasespace.pdf',bbox_inches='tight')
    
    plt.clf()
    for i in range(2):
        plt.subplot(2,1,i+1)
        if (i==0):
            values = X
            values_exact = X_exact
            values_prior = np.cos(t_values)+np.sin(t_values)
            y_label = r'$y(t)$'
        else:
            values = Y
            values_exact = Y_exact
            values_prior = -np.sin(t_values)+np.cos(t_values)
            y_label = r'$\dot{y}(t)$'
        ax = plt.gca()
        y_min = -2.0
        y_max = 2.0
        ax.set_xlim(0,1.5*T_final)
        ax.set_ylim(y_min,y_max)
        # --- 1 --- Numerical solution from neural network
        plt.plot(t_values,values,
                 linewidth=2,
                 color='blue',
                 label='network',
                 marker='o',
                 markerfacecolor='blue',
                 markeredgecolor='blue',
                 markersize=0)
        # --- 2 --- Exact solution from Adams-Bashforth integrator
        plt.plot(t_exact,
                 values_exact,
                 linewidth=2,
                 color='red',
                 marker='o',
                 markersize=0,
                 markerfacecolor='white',
                 markeredgecolor='red',
                 label='exact')
        # --- 3 --- Prior = solution for alpha=0
        plt.plot(t_values,
                 values_prior,
                 linewidth=2,
                 color='green',
                 marker='o',
                 markersize=0,
                 markerfacecolor='white',
                 markeredgecolor='green',
                 label='prior')
        plt.plot([T_final,T_final],[y_min,y_max],
                 linewidth=2,
                 color='black',
                 linestyle='-')
        plt.fill_between([T_final,1.5*T_final],
                         y_min,[y_max,y_max],
                         color='lightgray')
        ax.set_xlabel(r'time $t$')
        ax.set_ylabel(y_label)
    plt.legend(loc='lower left')
    plt.savefig('trajectory_timeseries.pdf',bbox_inches='tight')

def plot_history(losses,errors):
    '''Plot the convergence history of loss and error as a function of
    training epoch

    :arg losses: list with loss values
    :arg errors: list with error values
    '''
    epochs = np.asarray(range(len(losses)))
    plt.clf()
    plt.subplot(2,1,1)
    ax = plt.gca()
    ax.set_xlabel('epoch')
    ax.set_ylabel('loss')
    ax.set_yscale('log')
    plt.plot(epochs,losses,
             linewidth=2,
             color='blue')
    plt.subplot(2,1,2)
    ax = plt.gca()
    ax.set_xlabel('epoch')
    ax.set_ylabel('error')
    ax.set_yscale('log')
    plt.plot(epochs,errors,
             linewidth=2,
             color='red')
    plt.savefig('history.pdf',bbox_inches='tight')

def plot_loss_function(t_values,loss_values):
    '''Plot the local loss function \ell(t) = ||dy/dt-L(y)||_2^2
    
    :arg t_values: list with time points
    :arg loss_values: list with corresponding values of the loss function
    '''
    plt.clf()
    ax = plt.gca()
    ax.set_xlabel(r'time $t$')
    ax.set_ylabel(r'local loss $\ell(t) = ||\partial_t \vec{y}+\mathcal{L}\vec{y}||_2^2$')
    plt.plot(t_values,loss_values,
             linewidth=2,color='blue')
    plt.savefig('loss_function.pdf',bbox_inches='tight')
