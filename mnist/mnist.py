import numpy as np
from matplotlib import pyplot as plt
import tensorflow as tf
mnist = tf.keras.datasets.mnist

class MNISTFit(object):

    def __init__(self,
                 learning_rate=0.01,
                 batch_size=100,
                 training_epochs=100):
        self.import_data()
        print ('Size of training set = ',self.n)
        print ('Size of test set = ',self.n_test)
        print ('Size of images = '+str(self.n_x)+' x '+str(self.n_y)+' = '+str(self.m))
        self._learning_rate = learning_rate
        self._batch_size = batch_size
        self._training_epochs = training_epochs
        print ('Learning rate = ',self._learning_rate)
        print ('Batch size = ',self._batch_size)
        print ('training_epochs = ',self._training_epochs)
        self.y_pred = None
        
    # Import MNIST data set
    def import_data(self):
        (x_train_tmp,y_train_tmp),(x_test_tmp,y_test_tmp) = mnist.load_data()
        self.n = len(x_train_tmp)
        self.n_test = len(x_test_tmp)
        self.n_x, self.n_y = x_train_tmp[0].shape
        self.m = self.n_x*self.n_y
        # Number of different features ( = digits)
        self.k = 10
        # Create one-hot vector for training- and validation set
        self.y_train = np.zeros((self.n,self.k),dtype=int)
        for i,y in enumerate(y_train_tmp):
            self.y_train[i][y] = 1
        self.y_test = np.zeros((self.n_test,self.k),dtype=int)
        for i,y in enumerate(y_test_tmp):
            self.y_test[i][y] = 1
        # Create flattened training- and validation vectors
        self.x_train = np.zeros((self.n,self.m),dtype=float)
        for i in range(self.n):
            self.x_train[i,:] = np.array(x_train_tmp[i]).flatten()/255.
        self.x_test = np.zeros((self.n_test,self.m),dtype=float)
        for i in range(self.n_test):
            self.x_test[i,:] = np.array(x_test_tmp[i]).flatten()/255.

    def plot_training_data(self,first_idx,n_data):
        plt.clf()
        for i in range(n_data):
            plt.subplot(2,n_data,i+1)
            x_im = self.x_train[i+first_idx].reshape(self.n_x,self.n_y)
            ax = plt.gca()
            ax.set_xticks(())
            ax.set_yticks(())
            plt.imshow(x_im)
            plt.subplot(2,n_data,i+1+n_data)
            ax = plt.gca()
            plt.axis('off')
            ax.set_xticks(())
            ax.set_yticks(())
            ax.set_xlim(-1,1)
            ax.set_ylim(-2,0)
            ax.set_aspect('equal')
            plt.text(-.5,3,str(np.argmax(self.y_train[i+first_idx])),fontsize=32)
        plt.axis('off')
        plt.savefig('training_data.pdf')

    def plot_test_data(self,first_idx,n_data):
        plt.clf()
        for i in range(n_data):
            plt.subplot(4,n_data,i+1)
            x_im = self.x_test[i+first_idx].reshape(self.n_x,self.n_y)
            ax = plt.gca()
            ax.set_xticks(())
            ax.set_yticks(())
            plt.imshow(x_im)
            plt.subplot(4,n_data,i+1+n_data)
            ax = plt.gca()
            plt.axis('off')
            ax.set_xticks(())
            ax.set_yticks(())
            ax.set_xlim(-1,1)
            ax.set_ylim(-1,1)
            ax.set_aspect('equal')
            plt.text(-.5,0,str(np.argmax(self.y_test[i+first_idx])),fontsize=32)
            plt.subplot(4,n_data,i+1+2*n_data)
            ax = plt.gca()
            plt.axis('off')
            ax.set_xticks(())
            ax.set_yticks(())
            ax.set_xlim(-1,1)
            ax.set_ylim(-1,1)
            ax.set_aspect('equal')
            if (np.argmax(self.y_pred[i+first_idx])==np.argmax(self.y_test[i+first_idx])):
                c = 'green'
            else:
                c = 'red'
            plt.text(-.5,0,str(np.argmax(self.y_pred[i+first_idx])),fontsize=32,
                     color=c)
            plt.subplot(4,n_data,i+1+3*n_data)
            ax = plt.gca()
            ax.set_xticks(())
            ax.set_yticks(())
            ax.set_xlim(0,self.k)
            ax.set_ylim(0,1)
            plt.bar(np.array(range(self.k))+0.5,self.y_pred[i+first_idx])
        plt.savefig('test_data.pdf')

    def fit_data(self):
        # Train network with gradient descent
        x = tf.placeholder(tf.float32,[None,self.m])
        y = tf.placeholder(tf.float32,[None,self.k])

        W = tf.Variable(tf.zeros([self.m, self.k]))
        b = tf.Variable(tf.zeros([self.k]))

        # Number of batches
        n_batches = int(self.n / self._batch_size)
        
        # Softmax predictor function
        pred = tf.nn.softmax(tf.matmul(x,W)+b)
        # Cost function (log-likelihood)
        cost = tf.reduce_mean(-tf.reduce_sum(y*tf.log(pred),
                                             reduction_indices=1))
        # Optimizer (batched Gradient Descent) 
        optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)
        self._cost = []
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            for epoch in range(self._training_epochs):
                avg_cost = 0.0
                for i in range(n_batches):
                    batch_xs = self.x_train[i*batch_size:(i+1)*batch_size]
                    batch_ys = self.y_train[i*batch_size:(i+1)*batch_size]
                    _, c = sess.run([optimizer,cost],feed_dict={x:batch_xs,
                                                                y:batch_ys})
                    avg_cost += c / n_batches
                self._cost.append(avg_cost)
            # Work out accuracy of prediction
            correct_prediction = tf.equal(tf.argmax(pred, 1), tf.argmax(y, 1))
            accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
            fit_accuracy = accuracy.eval(feed_dict={x: self.x_test,
                                                    y: self.y_test})
            # Create prediction vector
            self.y_pred = pred.eval(feed_dict={x:self.x_test})

        print ('Fit accuracy = '+('%8.2f' % (100.*fit_accuracy))+' %')
    
        # Plot evolution of test function
        plt.clf()
        plt.plot(np.array(range(self._training_epochs))+1,
                 self._cost,
                 linewidth=2,
                 color='blue',
                 marker='o',
                 markersize=6)
        ax = plt.gca()
        ax.set_xlabel('epoch')
        ax.set_ylabel(r'cost function (log-likelihood $\log(J(\theta))$)')
        plt.savefig('cost.pdf',bbox_inches='tight')

if (__name__ == '__main__'):
    learning_rate=0.01
    batch_size=100
    training_epochs=20

    mnist = MNISTFit(learning_rate,batch_size,training_epochs)
    mnist.plot_training_data(100,10)
    mnist.fit_data()
    mnist.plot_test_data(500,10)
