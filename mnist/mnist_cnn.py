'''MNIST classification task with CNNs
'''

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# Imports
import sys
import numpy as np
import tensorflow as tf

tf.logging.set_verbosity(tf.logging.INFO)

# Use convolutional neural network?
# (use simple logistic regression otherwise) 
use_cnn = True

# Train neural network?
# (only test otherwise)
do_train = True

class Model(object):
    def __init__(self):
        self._label = ""

    @property
    def label(self):
        return self._label

    def model_fn(self,features, labels, mode):
        logits_layer = self.output_layer(features,mode)
        predictions = {
            "classes": tf.argmax(input=logits_layer, axis=1),
            "probabilities":tf.nn.softmax(logits_layer, name="softmax_tensor"),
            "logits_layer":logits_layer
        }
        if mode == tf.estimator.ModeKeys.PREDICT:
            return tf.estimator.EstimatorSpec(mode=mode,
                                              predictions=predictions)

        # Calculate loss
        loss = tf.losses.sparse_softmax_cross_entropy(labels=labels,
                                                      logits=logits_layer)

        # Configure the training op (for TRAIN mode)
        if mode == tf.estimator.ModeKeys.TRAIN:
            optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.001)
            train_op = optimizer.minimize(
                loss=loss,
                global_step=tf.train.get_global_step())
            return tf.estimator.EstimatorSpec(mode=mode,
                                              loss=loss,
                                              train_op=train_op)

        # Add evaluation metrics (for EVAL mode)
        eval_metric_ops = {
            "accuracy":tf.metrics.accuracy(
                labels=labels,
                predictions=predictions["classes"])}
        return tf.estimator.EstimatorSpec(
            mode=mode,
            loss=loss,
            eval_metric_ops=eval_metric_ops)
    
class ModelSoftmax(Model):

    def __init__(self):
        self._label="softmax"

    def output_layer(self,features,mode):
        """Single softmax layer"""
        # Input layer
        input_layer = tf.reshape(features["x"],[-1,784])
        # Logits layer
        logits_layer = tf.layers.dense(
            inputs=input_layer,
            units=10)
        return logits_layer
        
class ModelCNN(Model):

    def __init__(self):
        self._label="cnn"
    
    def output_layer(self,features,mode):
        """Model function for CNN"""
        # Input layer
        input_layer = tf.reshape(features["x"],[-1,28,28,1])

        # Convolutional layer #1
        conv_layer1 = tf.layers.conv2d(
            inputs=input_layer,
            filters=32,
            kernel_size=[5, 5],
            padding="same",
            activation=tf.nn.relu)

        # Pooling layer #1
        pool_layer1 = tf.layers.max_pooling2d(
            inputs=conv_layer1,
            pool_size=[2, 2],
            strides=2)
        
        # Convolutional layer #2
        conv_layer2 = tf.layers.conv2d(
            inputs=pool_layer1,
            filters=64,
            kernel_size=[5, 5],
            padding="same",
            activation=tf.nn.relu)

        # Pooling layer #2
        pool_layer2 = tf.layers.max_pooling2d(
            inputs=conv_layer2,
            pool_size=[2, 2],
            strides=2)

        # Flatten and Dense layer
        pool2_flat_layer = tf.reshape(
            pool_layer2,
            [-1, 7*7*64])
        dense_layer = tf.layers.dense(
            inputs=pool2_flat_layer,
            units=1024,
            activation=tf.nn.relu)

        # 40% dropout layer 
        dropout_layer = tf.layers.dropout(
            inputs=dense_layer,
            rate=0.4,
            training=(mode==tf.estimator.ModeKeys.TRAIN))

        # Logits layer
        logits_layer = tf.layers.dense(
            inputs=dropout_layer,
            units=10)
        return logits_layer

def plot_weights(b,W):
    from matplotlib import pyplot as plt
    plt.clf()
    for i in range(10):
        plt.subplot(2,5,i+1)
        plt.imshow(np.reshape(W[:,i],(28,28))+b[i],cmap='seismic')
        plt.axis('off')

    plt.savefig("weights.pdf",bbox_inches='tight')
    
def main(unused_argv):
    # Load training and eval data
    mnist = tf.contrib.learn.datasets.load_dataset("mnist")
    n_train = -1
    train_data = mnist.train.images[0:n_train,:]
    train_labels = np.asarray(mnist.train.labels[0:n_train], dtype=np.int32)
    eval_data = mnist.test.images
    eval_labels = np.asarray(mnist.test.labels, dtype=np.int32)
     # Create the estimator

    config = tf.estimator.RunConfig(
        session_config=tf.ConfigProto(log_device_placement=True))
    if (use_cnn):
        model = ModelCNN()
    else:
        model = ModelSoftmax()
    mnist_classifier = tf.estimator.Estimator(
        model_fn=model.model_fn,
        config=config,
        model_dir="./tmp/mnist_"+model.label+"_model")
    tensors_to_log = {"probabilities":"softmax_tensor"}
    train_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x":train_data},
        y=train_labels,
        batch_size=100,
        num_epochs=None,
        shuffle=True)
    if (do_train):
        mnist_classifier.train(
            input_fn=train_input_fn,
            steps=20000)

    eval_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x":eval_data},
        y=eval_labels,
        num_epochs=1,
        shuffle=False)
    eval_results = mnist_classifier.evaluate(
        input_fn=eval_input_fn)
    print(eval_results)
    print(mnist_classifier.get_variable_names())
    if not use_cnn:
        b = mnist_classifier.get_variable_value("dense/bias")
        W = mnist_classifier.get_variable_value("dense/kernel")
        plot_weights(b,W)

if __name__ == "__main__":
    tf.app.run()
