import numpy as np
import scipy.optimize
import math
from matplotlib import pyplot as plt

def create_data(data_list):
    '''Create data points in R^2, clustered around points with
    with associated weights.
    '''
    def create_point(x0,sigma):
        x = np.zeros(2)
        while True:
            dist2 = 0.0
            for i in range(2):
                x[i] = np.random.normal(x0[i],sigma)
                dist2 += (x[i]-x0[i])**2
            if (dist2 < sigma**2):
                return x
    n_total = 0
    for data in data_list:
        x0_,sigma_,y_,n_ = data
        n_total +=n_
    x = np.zeros((n_total,2))
    y = np.zeros(n_total)
    offset=0
    for data in data_list:
        x0_,sigma_,y_,n_ = data
        for i in range(n_):
            x[i+offset,:] = create_point(x0_,sigma_)
            y[i+offset] = y_
        offset += n_
    return x,y

class SVM(object):
    def __init__(self,x,y,kernel=np.dot,C_soft=1.0,use_scipy=True):
        '''Create new instance
        
        :arg x: Data array where x[i,:] is the position of the i-th point
        :arg y: Data array where y[i] is the class (+1 or -1) oth the i-th point
        :arg C_soft: Soft margin parameter C
        :arg use_scipy: Use scipy for dual minimisation problem
        '''
        self._x = np.array(x)
        self._y = np.array(y)
        # soft margin parameter C
        self._C_soft = C_soft
        self._use_scipy = use_scipy
        self.kernel = kernel
        self._n = len(self._y)

    def plot_data(self):
        '''Plot data together with separating line defined by w.x + b = 0
    
        '''
        p_lim = 10
        plt.clf()
        ax = plt.gca()
        ax.set_xlim(-p_lim,p_lim)
        ax.set_ylim(-p_lim,p_lim)
        plt.axes().set_aspect('equal')
        epsilon = 1.E-12
        # Contour plot of function
        X = np.arange(-p_lim,p_lim,0.1)
        Y = np.arange(-p_lim,p_lim,0.1)
        Xm,Ym = np.meshgrid(X,Y)
        Z = np.zeros((len(X),len(Y)))
        for i,x0 in enumerate(X):
            for j,x1 in enumerate(Y):
                x = (x0,x1)
                Z[j,i] = self.wdot(x)+self.b
        plt.contour(Xm,Ym,Z,levels=(-1,0,1),
                    colors=('black','green','black'),
                    linewidths=(2,2,2),
                    linestyles=('--','-','--'))
        plt.contourf(Xm,Ym,Z,cmap='bwr',levels=np.arange(-5,5,0.1))
        for alpha_,x_,y_ in zip(self.alpha,self._x,self._y):
            if (y_ > 0):
                color = 'red'
                marker = 's'
            else:
                color = 'blue'
                marker = 'o'
            edgecolor=color
            if ( (epsilon < alpha_) and (alpha_ < self._C_soft-epsilon) ):
                edgecolor='white'
            plt.plot((x_[0],),(x_[1],),
                     marker=marker,
                     markersize=4,
                     markerfacecolor=edgecolor,
                     markeredgecolor=color,
                     markeredgewidth=2,
                     color=color)
        plt.savefig('output.pdf',bbox_inches='tight')

    def minimise_coordinate_descent(self):
        n = len(self._y)
        M = np.zeros((n,n))
        for i in range(n):
            for j in range(n):
                M[i,j] = self._y[i]*self._y[j]*self.kernel(self._x[i,:],self._x[j,:])
        # Initial guess
        alpha = np.zeros(n)+0.5*self._C_soft
        # Function to minimise
        W = lambda alpha: 0.5*np.dot(np.dot(M,alpha),alpha)-np.sum(alpha)
        
        def minimise_1d(k,ell):
            '''Minimise W(\alpha_k) in coordinate direction k,
            such that the constraint is satisfied by adjusting \alpha_ell
            
            :arg k: Direction k
            :arg ell: Direction ell which is used to satisfy constraint
            '''
            n = len(alpha)
            C_kell = alpha[k]*self._y[k] + alpha[ell]*self._y[ell] - np.dot(alpha,self._y)
            sum_Mik = np.dot(M[:,k],alpha[:]) - M[k,k]*alpha[k] - M[k,ell]*alpha[ell] 
            sum_Miell = np.dot(M[:,ell],alpha[:]) - M[ell,k]*alpha[k] - M[ell,ell]*alpha[ell] 
            # Find coefficients of quadratic polynomial
            # w(alpha_k) = beta0 + beta1*alpha_k + beta2*alpha_k^2
            beta1 = M[k,ell]*self._y[ell]*C_kell - M[ell,ell]*self._y[k]*C_kell + sum_Mik - sum_Miell*self._y[k]*self._y[ell]-1.0 + self._y[k]*self._y[ell]
            beta2 = 0.5*(M[k,k] + M[ell,ell]) - M[k,ell]*self._y[k]*self._y[ell]
            # Find minimum
            alpha_k = -0.5*beta1/beta2
            # Now adjust minimum under constraints
            # 0 <= alpha_k <= C_{soft}, 0 <= alpha_ell <= C_{soft}
            if (self._y[k]*self._y[ell] > 0):
                b = min(self._C_soft,self._y[ell]*C_kell)
                a = max(0,self._y[ell]*C_kell - self._C_soft)
            else:
                b = min(self._C_soft,self._C_soft + self._y[ell]*C_kell)
                a = max(0,self._y[k]*C_kell)
            alpha_k = min(alpha_k,b)
            alpha_k = max(alpha_k,a)
            alpha_ell = self._y[ell]*(C_kell - self._y[k]*alpha_k)
            alpha[k] = alpha_k
            alpha[ell] = alpha_ell
            
        # Maximal number of iterations
        n_max = 1000
        w_objective_0 = W(alpha)
        w_objective = w_objective_0
        for i in range(n_max):
            w_objective_old = w_objective
            for k in range(n):
                ell = k
                while ell == k:
                    ell = np.random.randint(n)
                minimise_1d(k,ell)
            w_objective = W(alpha)
            rho = abs(w_objective/w_objective_old)
            #print ('%8d' % i) +' : '+('%12.2f'% w_objective)+' '+('%12.6f' % rho)+' '+('%12.8e' % (np.linalg.norm(alpha-alpha_old)/np.linalg.norm(alpha)))
        return alpha
    
    def minimise_scipy(self):
        '''Minimise the cost function using scipy's minise method'''
        n = len(self._x)
        M = np.zeros((n,n))
        for i in range(n):
            for j in range(n):
                M[i,j] = self._y[i]*self._y[j]*self.kernel(self._x[i,:],self._x[j,:])
        # Function to minimise
        W = lambda alpha: 0.5*np.dot(np.dot(M,alpha),alpha)-np.sum(alpha)
        # Jacobian
        JacW = lambda alpha: np.dot(M,alpha)-1.0
        # Initial guess: constant vector with ones
        alpha0 = np.zeros(n)+0.5*self._C_soft
        constraints={'type': 'eq', 'fun': lambda alpha: np.dot(alpha,self._y)}
        bounds = n*[(0.0,self._C_soft)]
        res = scipy.optimize.minimize(W,alpha0,
                                      method='SLSQP',
                                      jac=JacW,
                                      bounds=bounds,
                                      constraints=constraints)
        if (not res.success):
            print 'ERROR: Could not maximise W(alpha)'
        return res.x
        
    def solve_dual(self):
        '''Solve the dual optimisation problem, i.e. minimise the following
        function of the vector alpha:
        
        J(alpha) = 0.5*sum_{i,j=0}^{n-1} \alpha_i*y_i*y_j*dot(x_i,x_j) - sum_{i=0}^{n-1} alpha_i
        
        Under the constraint that 0 <= alpha_i <= C_{soft} 
    
        '''
        if (self._use_scipy):
            self.alpha = self.minimise_scipy()
        else:
            self.alpha = self.minimise_coordinate_descent()
        n = len(self._x)
        # Find support vectors
        epsilon = 1.E-12
        x_support = self._x[(epsilon<self.alpha) & (self.alpha<self._C_soft-epsilon),:]
        y_support = self._y[(epsilon<self.alpha) & (self.alpha<self._C_soft-epsilon)]
        for x_,y_ in zip(x_support,y_support):
            self.b = y_ - self.wdot(x_)
            
    def wdot(self,x):
        tmp = 0.0
        for i in range(self._n):
            tmp += self.alpha[i]*self._y[i]*self.kernel(self._x[i,:],x)
        return tmp
        

class KernelDot(object):
    def __call__(self,x1,x2):
        return np.dot(x1,x2)

class KernelRBF(object):
    def __init__(self,sigma):
        self._sigma = sigma
        self._gamma = 1./(2.*self._sigma**2)
    def __call__(self,x1,x2):
        dx = x1-x2
        dx2 = np.dot(dx,dx)
        return math.exp(-self._gamma*dx2)

class KernelPolynomial(object):
    def __init__(self,d,offset):
        self._d = d
        self._offset = offset
    def __call__(self,x1,x2):
        return (np.dot(x1,x2)+self._offset)**self._d

if (__name__ == '__main__'):
    np.random.seed(13310417)
    n = 32
    x,y = create_data( ( ((3,4),2.0,+1,n),
                         ((6,-5),1.0,+1,n),
                         ((7,0),2.0,-1,n),
                         ((2,-2),1.0,-1,n),
                         ((-2,-3),2.0,+1,n),
                         ((-2,4),2.0,-1,n)) )
    sigma = 1.0
    kernel = KernelRBF(2)
#    kernel = np.dot
    svm = SVM(x,y,kernel=kernel,C_soft=10)
    svm.solve_dual()
    svm.plot_data()
