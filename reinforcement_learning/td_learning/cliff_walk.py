import numpy as np
from matplotlib import pyplot as plt

class GridWorld(object):
    def __init__(self, nx, ny):
        self.nx = nx
        self.ny = ny

    def step(self,state,action):
        '''
        Take action (i.e. step in a particular direction) from current state
        and return reward

        :arg state: current state (i.e. position (i,j) in grid)
        :arg action: direction of travel, i.e. 'N','S','E','W', encoded as
           (0,1,2,3)
        '''
        offset = {0:( 0,-1),
                  1:( 0,+1),
                  2:(+1, 0),
                  3:(-1, 0)}
        state_new = np.asarray(state) + np.asarray(offset[action])
        reward = -1
        terminal = False
        if (state_new[1] == 0):
            if (state_new[0] == self.nx-1):
                # Hit goal state
                state_new = np.asarray((0,0))
                terminal = True
            if (state_new[0] > 0) and (state_new[0] < self.nx-1):
                # Hit cliff
                state_new = np.asarray((0,0))
                reward = -100
                terminal = True
        if (state_new[0] < 0):
            state_new[0] += 1
        if (state_new[0] > self.nx-1):
            state_new[0] -= 1
        if (state_new[1] < 0):
            state_new[1] += 1
        if (state_new[1] > self.ny-1):
            state_new[1] -= 1
        return state_new, reward, terminal

class Learner(object):
    def __init__(self,gridworld,alpha):
        self.gridworld = gridworld
        self.nx = self.gridworld.nx
        self.ny = self.gridworld.ny
        self.Q = np.zeros((self.nx, self.ny, 4))
        self.nvisits = np.zeros((self.nx, self.ny))
        self.alpha = alpha

    def show_Q(self):
        action_dict = ('N','S','E','W')
        for i in range(self.ny):
            s = ''
            for j in range(self.nx):
                s+= action_dict[np.argmax(self.Q[j,i,:])]
            print (s)

    def eps_greedy_action(self,state):
        '''Select epsilon-greedy action'''
        actions = self.Q[state[0],state[1],:]
        i_max = np.random.choice(np.flatnonzero(actions == actions.max()))
        xi = np.random.uniform(0.,1.)
        s = 0.0
        for i in range(4):
            if (i == i_max):
                s += 1.-0.75*self.epsilon
            elif (i < 3):
                s += 0.25*self.epsilon
            else:
                s = 1.0
            if (xi <= s):
                return i

    def greedy_action(self,state):
        '''Select greedy action'''
        actions = self.Q[state[0],state[1],:]
        i_max = np.random.choice(np.flatnonzero(actions == actions.max()))
        return i_max
    
class SARSA(Learner):
    def __init__(self,gridworld,alpha=0.1,epsilon=0.1):
        super().__init__(gridworld,alpha)
        self.epsilon=epsilon

    def learn(self,nepisodes):
        self.Q[:,:,:] = 0.25
        episode = 0
        self.rewards = []
        self.nvisits[:,:] = 0.0
        while (episode < nepisodes):
            state = (0,0)
            action = self.eps_greedy_action(state)
            terminal = False
            tmp_reward = 0.0
            while not terminal:
                self.nvisits[state[0],state[1]] += 1
                state_new, reward, terminal = self.gridworld.step(state,action)
                tmp_reward += reward
                action_new = self.eps_greedy_action(state_new)
                if (terminal):
                    Q_new = 0.0
                else:
                    Q_new = self.Q[state_new[0],state_new[1],action_new]
                self.Q[state[0],state[1],action] *= (1.-self.alpha)
                self.Q[state[0],state[1],action] += self.alpha*(reward + Q_new)  
                state = state_new
                action = action_new
            episode += 1
            self.rewards.append(tmp_reward)

class QLearning(Learner):
    def __init__(self,gridworld,alpha=0.1,epsilon=0.1):
        super().__init__(gridworld,alpha)
        self.epsilon = epsilon

    def learn(self,nepisodes):
        self.Q[:,:,:] = 0.25
        episode = 0
        self.rewards = []
        self.nvisits[:,:] = 0.0
        while (episode < nepisodes):
            state = (0,0)
            terminal = False
            tmp_reward = 0.0
            while not terminal:
                self.nvisits[state[0],state[1]] += 1
                action = self.eps_greedy_action(state)
                state_new, reward, terminal = self.gridworld.step(state,action)
                tmp_reward += reward
                if (terminal):
                    Q_new = 0.0
                else:
                    Q_new = np.max(self.Q[state_new[0],state_new[1],:])
                self.Q[state[0],state[1],action] *= (1.-self.alpha)
                self.Q[state[0],state[1],action] += self.alpha*(reward + Q_new)
                state = state_new
            episode += 1
            self.rewards.append(tmp_reward)

def smooth(Y,k):
    n = len(Y)
    Y_new = np.zeros(n)
    for i in range(n):
        Y_new[i] = np.average(Y[i:i+min(i+k,n)])
    return Y_new

if (__name__ == '__main__'):
    np.random.seed(36119217)
    gridworld = GridWorld(20,8)
    alpha = 0.1
    epsilon = 0.2
    nepisodes = 10000
    print ('alpha     = ',alpha)
    print ('epsilon   = ',epsilon)
    print ('nepisodes = ',nepisodes)
    print ()
    print ('=== SARSA ===')
    print ()
    sarsa = SARSA(gridworld,alpha,epsilon)
    sarsa.learn(nepisodes)
    sarsa.show_Q()
    print ()
    print ('=== Q-Learning ===')
    print ()
    qlearning = QLearning(gridworld,alpha,epsilon)
    qlearning.learn(nepisodes)
    qlearning.show_Q()

    k = 20
    plt.clf()
    ax = plt.gca()
    ax.set_xlabel('episode')
    ax.set_ylabel('sum of rewards during episode')
    X = np.arange(0,nepisodes)
    plt.plot(X,smooth(sarsa.rewards,k),
             linewidth=2,color='blue',label='SARSA')
    plt.plot(X,smooth(qlearning.rewards,k),
             linewidth=2,color='red',label='Q-learning')
    plt.legend(loc='lower right')
    ax.set_ylim(-100,0)
    ax.set_xlim(0,nepisodes-k)
    plt.savefig('rewards.pdf',bbox_inches='tight')

    plt.clf()
    plt.subplot(2,1,1)
    plt.imshow(sarsa.nvisits.transpose(),cmap='seismic')
    ax = plt.gca()
    ax.set_xlabel('SARSA')
    plt.subplot(2,1,2)
    plt.imshow(qlearning.nvisits.transpose(),cmap='seismic')
    ax = plt.gca()
    ax.set_xlabel('Q-learning')
    plt.savefig('visits.pdf',bbox_inches='tight')
