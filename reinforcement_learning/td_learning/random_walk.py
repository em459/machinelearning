import numpy as np
from matplotlib import pyplot as plt

'''
===============================================================
Implementation of Example 6.2 in the book
"Reinforcement Learning: An introduction" by Sutton & Barto
===============================================================
'''

n_states = 5
V_exact = (np.arange(n_states)+1)/(1.*n_states+1.)

def generate_episode():
    state = int((n_states-1)/2)
    traj = []
    while (state >=0) and (state<n_states):
        traj.append(state)
        state += 2*np.random.randint(0,2)-1
    if (state < 0):
        reward = 0
    else:
        reward = 1
    return traj, reward

def monte_carlo(n_episodes,alpha):
    V = np.zeros(n_states)+0.5
    error = []
    error.append(np.sqrt(np.average((V-V_exact)**2)))
    for k in range(n_episodes):
        traj, reward = generate_episode()
        for state in range(n_states):
            if state in traj:
                V[state] += alpha*(reward-V[state])
        error.append(np.sqrt(np.average((V-V_exact)**2)))
    return np.asarray(V), np.asarray(error)

def td_zero(n_episodes,alpha):
    V = np.zeros(n_states)+0.5
    error = []
    error.append(np.sqrt(np.average((V-V_exact)**2)))
    for k in range(n_episodes):
        traj, reward = generate_episode()
        for i in range(len(traj)):
            state = traj[i]
            if i < len(traj)-1:
                state_new = traj[i+1]
                V[state] += alpha*(V[state_new]-V[state])
            else:
                V[state] += alpha*(reward-V[state])
        error.append(np.sqrt(np.average((V-V_exact)**2)))
    return np.asarray(V), np.asarray(error)

np.random.seed(21491417)
n_episodes = 100
alpha_mc = 0.02
alpha_td = 0.02

V_mc, error_mc = monte_carlo(n_episodes,alpha_mc)
    
V_td, error_td = td_zero(n_episodes,alpha_td)
X = np.arange(0,n_episodes+1)

plt.clf()
plt.plot(X,error_mc,linewidth=2,color='blue',
         label=r'Monte Carlo $\alpha='+('%5.3f' % alpha_mc)+'$')
plt.plot(X,error_td,linewidth=2,color='red',
         label=r'TD(0) $\alpha='+('%5.3f' % alpha_td)+'$')
ax = plt.gca()
ax.set_xlabel('number of episodes')
ax.set_ylabel('error')
plt.legend(loc='upper right')
plt.savefig('error.pdf',bbox_inches='tight')
