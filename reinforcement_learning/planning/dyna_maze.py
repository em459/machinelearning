import numpy as np
from matplotlib import pyplot as plt
import queue

class Maze(object):

    def __init__(self):
        self.directions = np.asarray(((0,-1),(0,+1),(+1,0),(-1,0)))
        
    def step(self,state,action):
        '''At a given state, take a particular action, i.e. walk 
        N, S, E or W

        :arg state: Current state in the form (i,j)
        :arg action: direction, i.e. a number between 0 and 3
        '''        
        state_new = np.asarray(state)+self.directions[action]
        reward = 0
        hit_wall = False
        finished_episode = False
        # Hitting outside wall?
        if (state_new[0] < 0) or (state_new[0] > self.nx-1) or (state_new[1] < 0) or (state_new[1] > self.ny-1):
            hit_wall = True
        for x in self.blocked_squares:
            if (state_new==x).all(): 
                hit_wall = True
        if (hit_wall):
            state_new[0] = state[0]
            state_new[1] = state[1]
        if (state_new==self.goal_state).all():
            state_new = self.initial_state
            reward = 1.0
            finished_episode = True
        return state_new, reward, finished_episode

    def linearise(self,state):
        '''Convert state into an index, i.e. return i + nx*j

        :arg state: state in the form (i,j)
        '''
        return state[0] + self.nx*state[1]

    def unlinearise(self,linstate):
        '''Convert index into a state, i.e. given k, work out 
        i,j such that i + nx*j = k
        
        :arg state: state in the form
        '''
        j = int(linstate/self.nx)
        i = linstate - self.nx*j
        return (i,j)

    def plot(self,filename):
        '''Plot maze to file
        :arg filename: Name of file to write to
        '''
        plt.clf()
        maze_data = np.zeros((self.nx,self.ny))
        for i in range(self.nx):
            for j in range(self.ny):
                for x in self.blocked_squares:
                    if (x[0]==i) and (x[1]==j):
                        maze_data[i,j] = 1.0
        i,j = self.goal_state
        maze_data[i,j] = 0.5
        i,j = self.initial_state
        maze_data[i,j] = 0.5
        plt.imshow(maze_data.transpose(),interpolation='none')        
        plt.savefig(filename,bbox_inches='tight')


class SmallMaze(Maze):
    def __init__(self):
        '''9 x 6 maze as in Fig. 8.3 of Sutton & Barto'''
        super().__init__()
        self.nx = 9
        self.ny = 6
        self.blocked_squares = ((2,1),(2,2),(2,3),
                                (5,4),
                                (7,0),(7,1),(7,2))
        self.initial_state = (0,2)
        self.goal_state = (8,0)

class LargeMaze(Maze):
    def __init__(self,scaling_factor):
        '''(9s) x (6s) maze as in Fig. 8.3 of Sutton & Barto

        :arg scaling_factor: scaling factor s
        '''
        super().__init__()
        self.scaling_factor = scaling_factor
        self.nx = 9*self.scaling_factor
        self.ny = 6*self.scaling_factor
        self.blocked_squares_coarse = ((2,1),(2,2),(2,3),
                                       (5,4),
                                       (7,0),(7,1),(7,2))
        self.blocked_squares = []
        for x in self.blocked_squares_coarse:
            offset = self.scaling_factor*np.asarray(x)
            for i in range(self.scaling_factor):
                for j in range(self.scaling_factor):
                    self.blocked_squares.append(offset+np.asarray((i,j)))
        self.initial_state = (0,2*self.scaling_factor)
        self.goal_state = (8*self.scaling_factor,0)

class Learner(object):
    def __init__(self,maze,n=10,alpha=0.1,epsilon=0.1,gamma=0.95,
                 seed_plan=12341247,seed_learn=1241753):
        '''Abstract base class for learner
        :arg maze: maze object
        :arg n: number of planning steps per iteration
        :arg alpha: Update parameter (learning rate)
        :arg epsilon: parameter for epsilon-greedy action selection
        :arg gamma: discount factor
        '''
        self.n = n
        self.alpha = alpha
        self.epsilon = epsilon
        self.gamma = gamma
        self.model = {}
        self.maze = maze
        self.Q = np.zeros((self.maze.nx, self.maze.ny, 4))
        self.plan_rng = np.random.RandomState(seed_plan)
        self.learn_rng = np.random.RandomState(seed_learn)

    def update_Q(self,state,state_new,action,reward):
        '''Update Q(S,A)'''
        dQ = reward + self.gamma*np.max(self.Q[state_new[0],state_new[1],:])-self.Q[state[0],state[1],action]
        self.Q[state[0],state[1],action] += self.alpha*dQ
            
    def eps_greedy_action(self,state):
        '''Select epsilon-greedy action'''
        actions = self.Q[state[0],state[1],:]
        i_max = self.learn_rng.choice(np.flatnonzero(actions == actions.max()))
        xi = self.learn_rng.uniform(0.,1.)
        s = 0.0
        for i in range(4):
            if (i == i_max):
                s += 1.-0.75*self.epsilon
            elif (i < 3):
                s += 0.25*self.epsilon
            else:
                s = 1.0
            if (xi <= s):
                return i
            
    def plot_optimal_path(self,filename):
        '''Plot optimal path in maze and save output in file

        :arg filename: Name of file to plot to
        '''
        plt.clf()
        maze_data = np.zeros((self.maze.nx,self.maze.ny))
        for i in range(self.maze.nx):
            for j in range(self.maze.ny):
                for x in self.maze.blocked_squares:
                    if (x[0]==i) and (x[1]==j):
                        maze_data[i,j] = 1.0
        i,j = self.maze.goal_state
        maze_data[i,j] = 0.5
        i,j = self.maze.initial_state
        maze_data[i,j] = 0.5
        path_x = [i]
        path_y = [j]
        state = (i,j)
        while not (np.asarray(state)==self.maze.goal_state).all():
            action = np.argmax(self.Q[state[0],state[1],:])
            state, _, finished_episode = self.maze.step(state,action)
            if (finished_episode):
                state = self.maze.goal_state
            path_x.append(state[0])
            path_y.append(state[1])
        plt.plot(path_x,path_y,linewidth=4,color='black')
        plt.imshow(maze_data.transpose(),interpolation='none')        
        plt.savefig(filename,bbox_inches='tight')

        
class DynaQ(Learner):
    def __init__(self,maze,n=10,alpha=0.1,epsilon=0.1,gamma=0.95,
                 seed_plan=12341247,seed_learn=1241753):
        '''
        :arg maze: maze object
        :arg n: number of planning steps per iteration
        :arg alpha: Update parameter (learning rate)
        :arg epsilon: parameter for epsilon-greedy action selection
        :arg gamma: discount factor
        '''
        super().__init__(maze,n,alpha,epsilon,gamma,seed_plan,seed_learn)
        print ('*** Dyna-Q learner ***')
        print ('')
        print (('  n       = (%8d)' % self.n))
        print (('  alpha   = (%8.3f)' % self.alpha))
        print (('  epsilon = (%8.3f)' % self.epsilon))
        print (('  gamma   = (%8.3f)' % self.gamma))
        print ('')
        

    def learn(self,max_episodes):
        '''Execute Dyna-Q learning- and planning algorithm

        :arg max_episodes: Number of episodes to execute
        '''
        self.Q[:,:,:] = 0.0
        self.model = {}
        state = maze.initial_state
        self.steps_per_episode = []
        nsteps = 0
        episode = 0
        while (episode < max_episodes):
            action = self.eps_greedy_action(state)
            state_new, reward, finished_episode = self.maze.step(state,action)
            self.update_Q(state,state_new,action,reward)
            nsteps += 1
            if (finished_episode):
                self.steps_per_episode.append(nsteps)
                nsteps = 0
                episode += 1
            lin_state = self.maze.linearise(state)
            if (lin_state in self.model.keys()):
                self.model[lin_state][action] = (reward, state_new)
            else:
                self.model[lin_state] = {action: (reward, state_new)}
            state = state_new
            # Planning stage
            for k in range(self.n):
                lin_state = self.plan_rng.choice(list(self.model.keys()))
                action_ = self.plan_rng.choice(list(self.model[lin_state].keys()))
                state_ = self.maze.unlinearise(lin_state)
                reward_, state_new_ = self.model[lin_state][action_]
                self.update_Q(state_,state_new_,action_,reward_)

class PrioritizedSweeping(Learner):
    def __init__(self,maze,n=10,alpha=0.1,epsilon=0.1,gamma=0.95,theta=0.1,
                 seed_plan=12341247,seed_learn=1241753):
        '''
        :arg maze: maze object
        :arg n: number of planning steps per iteration
        :arg alpha: Update parameter (learning rate)
        :arg epsilon: parameter for epsilon-greedy action selection
        :arg gamma: discount factor
        :arg theta: threshold for insertion into priority queue
        '''
        super().__init__(maze,n,alpha,epsilon,gamma,seed_plan,seed_learn)
        self.theta = theta
        print ('*** Prioritized Sweeping learner ***')
        print ('')
        print (('  n       = (%8d)' % self.n))
        print (('  alpha   = (%8.3f)' % self.alpha))
        print (('  epsilon = (%8.3f)' % self.epsilon))
        print (('  gamma   = (%8.3f)' % self.gamma))
        print (('  theta   = (%8.3f)' % self.theta))
        print ('')
        
    def dQ_abs(self,state,state_new,action,reward):
        '''return |dQ(S,A)|'''
        dQ = reward + self.gamma*np.max(self.Q[state_new[0],state_new[1],:])-self.Q[state[0],state[1],action]
        return abs(dQ)

    def learn(self,max_episodes):
        '''Execute Prioritized Sweeping learning- and planning algorithm

        :arg max_episodes: Number of episodes to execute
        '''
        self.Q[:,:,:] = 0.0
        self.model = {}
        state = maze.initial_state
        self.steps_per_episode = []
        nsteps = 0
        episode = 0
        pqueue = queue.PriorityQueue()
        predecessors = {}
        while (episode < max_episodes):
            action = self.eps_greedy_action(state)
            state_new, reward, finished_episode = self.maze.step(state,action)
            self.update_Q(state,state_new,action,reward)
            nsteps += 1
            if (finished_episode):
                self.steps_per_episode.append(nsteps)
                nsteps = 0
                episode += 1
            # Add (S,A) to model
            lin_state = self.maze.linearise(state)
            if (lin_state in self.model.keys()):
                self.model[lin_state][action] = (reward, state_new)
            else:
                self.model[lin_state] = {action: (reward, state_new)}
            # Add (S,A) to predecessor list of S'
            lin_state_new = self.maze.linearise(state_new)
            if (lin_state_new in predecessors.keys()):
                predecessors[lin_state_new].add((state[0],state[1],action))
            else:
                predecessors[lin_state_new] = set(((state[0],state[1],action),))
            dQabs = self.dQ_abs(state,state_new,action,reward)
            if (dQabs > self.theta):
                pqueue.put((-dQabs,lin_state,action))
            state = state_new
            # Planning stage
            k = 0
            while (not pqueue.empty()) and (k < self.n):
                _, lin_state_, action_ = pqueue.get()
                state_ = self.maze.unlinearise(lin_state_)
                reward_, state_new_ = self.model[lin_state_][action_]
                self.update_Q(state_,state_new_,action_,reward_)
                if lin_state_ in predecessors.keys():                    
                    for prec in predecessors[lin_state_]:
                        prev_state_i, prev_state_j, prev_action_ = prec
                        prev_state_ = (prev_state_i, prev_state_j)
                        lin_prev_state_ = self.maze.linearise(prev_state_)
                        reward_, _ = self.model[lin_prev_state_][prev_action_]
                        dQabs = self.dQ_abs(prev_state_,
                                            state_,
                                            prev_action_,
                                            reward_)
                        if (dQabs > self.theta):
                            pqueue.put((-dQabs,lin_prev_state_,prev_action_))
                k += 1
                
if (__name__ == '__main__'):
    maze = LargeMaze(4)
    maze.plot('maze.pdf')
    max_episodes = 100
    alpha = 0.1
    epsilon = 0.1
    gamma = 0.95
    theta = 0.01
    plt.clf()
    for n in (5,):
        dynaq = DynaQ(maze,n,alpha,epsilon,gamma)
        dynaq.learn(max_episodes)
        Y = dynaq.steps_per_episode
        X = np.arange(len(Y))
        plt.plot(X,Y,linewidth=2,label=r'Dyna-Q $n='+str(n)+r'$')
        print ('n = ', n, ' Y = ',Y)
    for n in (5,):
        sweeper = PrioritizedSweeping(maze,n,alpha,epsilon,gamma,theta)
        sweeper.learn(max_episodes)
        Y = sweeper.steps_per_episode
        X = np.arange(len(Y))
        plt.plot(X,Y,linewidth=2,label=r'Prioritized Sweeping $n='+str(n)+r'$')
        print ('n = ', n, ' Y = ',Y)
    ax = plt.gca()
    ax.set_ylim(0,800)
    ax.set_xlabel('episode')
    ax.set_ylabel('steps per episode')
    plt.legend(loc='upper right')
    plt.savefig('steps_per_episode.pdf',bbox_inches='tight')
    dynaq.plot_optimal_path('optimal_path.pdf')
