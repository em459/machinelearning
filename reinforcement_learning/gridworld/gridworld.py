from __future__ import print_function
from matplotlib import pyplot as plt
import matplotlib.patches as patches

'''
===========================================================
Robot moving around a small maze, solve Bellman optimality
equations to find optimal value function and policy.
===========================================================
'''

import numpy as np

class Maze(object):
    '''Defines a maze of size nx x ny. The interior cells are 
    in the range (1,nx) x (1,ny), and we also store the wall cells on the
    boundary.

    Each cell can have three values:
    
    0 = free cell
    1 = wall cell
    2 = cell with positive reward
    3 = cell with negative reward

    Note that there can also be wall cells in the interior

    This class also stores the reward for each cell, i.e. the reward that the
    robot receives if it enters this cell.

    :arg nx: Number of interior cells in x-direction
    :arg ny: Number of interior cells in y-direction
    :arg cost_move: Cost of a move
    :arg cost_wall: Cost of bumping into a wall
    '''
    def __init__(self,nx,ny,
                 cost_move=-0.1,
                 cost_wall=-1.0):
        self.nx = nx
        self.ny = ny
        self.cost_move = cost_move
        self.cost_wall = cost_wall
        # Initialise cell and reward arrays
        self.cell = np.zeros((nx+2,ny+2))
        self.reward = np.zeros((nx+2,ny+2))
        self.reward[:,:] = self.cost_move
        self.cell[:,:] = 0 
        # Add Walls
        self.cell[0,:] = 1
        self.cell[-1,:] = 1
        self.cell[:,0] = 1
        self.cell[:,-1] = 1        
        self.reward[0,:] = self.cost_wall
        self.reward[-1,:] = self.cost_wall
        self.reward[:,0] = self.cost_wall
        self.reward[:,-1] = self.cost_wall

    def add_wall(self,ix,iy):
        '''Add a wall cell at position (ix,iy)
        
        :arg ix: x-position
        :arg iy: y-position
        '''
        self.reward[ix,iy] = self.cost_wall
        self.cell[ix,iy] = 1

        
    def add_reward(self,ix,iy,reward):        
        '''Add a reward cell at position (ix,iy)
        
        :arg ix: x-position
        :arg iy: y-position
        :arg reward: Value of immediate reward for this cell
        '''
        self.reward[ix,iy] = reward
        if (reward >= 0):
            self.cell[ix,iy] = 2
        else:
            self.cell[ix,iy] = 3

    def show(self):
        '''Display the maze, i.e. print it to screen'''
        conversion_table = {0:'.',1:'#',2:'+',3:'-'}
        for iy in range(self.ny+2):
            s = ''
            for ix in range(self.nx+2):
                s += conversion_table[self.cell[ix,iy]]
            print (s)

class Transitions(object):
    '''Class which stores the transition probabilities for the maze.

    Basically, this encodes the function

    p(s',r|s,a) = Pr[S_t=s', R_t=r | S_{t-1}=s, A_{t-1}=a]

    At each cell, the robot can move in one of the four directions
    a = {N,S,E,W}. It will transition to the target square (i',j')
    with a fixed probability p_straight, and veer of to one of the adjacent
    directions with probability (1-p_straight)/2. E.g. if it is at cell
    (i,j) and the action is a=E, then it moves to (i+1,j) with probability
    p_straight and to (i,j-1) or (i,j+1) with probability (1-p_straight)/2
    each.

    If it bounces into a wall it will stay where it is.

    This class builds up a matrix of the following form:
    For each cell (i,j) of the grid it stores a dictionary

    {a_1:( (i'_{1,1},j'_{1,1}},r_{1,1},p_{1,1}), 
           (i'_{1,2},j'_{1,2}},r_{1,2},p_{1,2}), 
           ...
           (i'_{2,k},j'_{2,k}},r_{2,k},p_{2,k}) ),
     a_2:( (i'_{2,1},j'_{2,1}},r_{2,1},p_{2,1}), 
           (i'_{2,2},j'_{2,2}},r_{2,2},p_{2,2}), 
           ...
           (i'_{2,k},j'_{2,k}},r_{2,k},p_{2,k}) ),
     ...
     a_n:( (i'_{n,1},j'_{n,1}},r_{n,1},p_{n,1}), 
           (i'_{n,2},j'_{n,2}},r_{n,2},p_{n,2}), 
           ...
           (i'_{n,k},j'_{n,k}},r_{n,k},p_{n,k}) }

    (here n=4, k=3, since there are four possible actions and three possible
     end states)

    We have that: p_{b,c} = p((i'_{b,c},j'_{b,c}),r_{b,c}|(i,j),a_b), i.e.
    this data structure encodes all necessary information for the finite 
    Markov Decision process which descibes navigation through the maze

    :arg maze: Object describing the maze in which the robot moves
    :arg p_straight: Probability of going straight (see above)

    '''
    def __init__(self,maze,p_straight=0.8):
        self.maze = maze
        self.nx = maze.nx
        self.ny = maze.ny
        self.p_straight = p_straight
        self.build_moves()
        
    def build_moves(self):
        '''Build the matrix dictionary which encodes p(s',r|s,a)'''
        # Neighbours offsets for all directions (=actions)
        offset = {'N':(( 0,-1),(+1, 0),(-1, 0)),
                  'S':(( 0,+1),(+1, 0),(-1, 0)),
                  'E':((+1, 0),( 0,+1),( 0,-1)),
                  'W':((-1, 0),( 0,+1),( 0,-1))}
        # Probabilities of moving in a given direction
        prob = (self.p_straight,
                0.5*(1.0-self.p_straight),
                0.5*(1.0-self.p_straight))
        # Loop over all cells in the maze
        moves = []
        for iy in range(self.ny+2):
            row_moves = []
            for ix in range(self.nx+2):
                # For each cell, build the dictionary
                action_dict = {}
                if self.maze.cell[ix,iy] == 0:
                    # Loop over all actions = directions
                    for a in ('N','S','E','W'):
                        action_dict[a] = []
                        # Loop over all possible neighbours defined by action
                        for i in range(3):
                            # Work out next cell
                            ix_nb = ix + offset[a][i][0]
                            iy_nb = iy + offset[a][i][1]
                            # Probability
                            p = prob[i]
                            # If neighbour is wall, stay where you are
                            if self.maze.cell[ix_nb,iy_nb] == 1:
                                ix_new = ix
                                iy_new = iy
                                r = self.maze.cost_wall
                            else:
                                # Otherwise move to next cell
                                ix_new = ix_nb
                                iy_new = iy_nb
                                r = self.maze.cost_move
                            action_dict[a].append((ix_new,iy_new,r,p))
                if (self.maze.cell[ix,iy] > 1):
                    # Stay where you are on a reward cell
                    action_dict = {'0':((ix,iy,self.maze.reward[ix,iy],1.0),)}
                row_moves.append(action_dict)
            moves.append(row_moves)
        self.moves = moves

class MazeOptimizer(object):
    '''Class for finding the optimal value function of a maze

    :arg maze: Maze to optimize
    '''
    def __init__(self,maze):
        self.maze = maze
        self.transitions = Transitions(self.maze)
        self.nx = self.maze.nx
        self.ny = self.maze.ny

    def value_function(self,gamma):
        '''Calculate optimal value function for a given discount factor
        
        :arg gamma: discount factor
        '''
        # Value function
        value = np.zeros((self.nx+2,self.ny+2))
        # Optimal action in each cell
        opt_action = np.zeros((self.nx+2,self.ny+2),dtype=int)
        # set to defined values
        for iy in range(self.ny+2):
            for ix in range(self.nx+2):
                if (self.maze.cell[ix,iy] == 2) or (self.maze.cell[ix,iy] == 3):
                    value[ix,iy] = self.maze.reward[ix,iy]
        maxiter = 100
        # Recursive solve Bellmann optimality equations
        for k in range(maxiter):
            for iy in range(1,self.ny+1):
                for ix in range(1,self.nx+1):
                    if self.maze.cell[ix,iy] == 0:
                        action_dict = self.transitions.moves[iy][ix]
                        action_value = []
                        for action in ('N','S','E','W'):
                            # Loop over all actions and find optimal value
                            av = 0.0
                            for item in action_dict[action]:
                                ix_nb, iy_nb, r, p = item
                                av += p*(r+gamma*value[ix_nb,iy_nb])
                            action_value.append(av)
                        value[ix,iy] = np.max(action_value)
                        opt_action[ix,iy] = np.argmax(action_value)
        self.value = value
        self.opt_action = opt_action

    def plot(self):
        '''Plot the optimal value function and optimal policy'''
        if (self.value is None):
            return
        dir_list = ('N','S','E','W')
        x = {'N':0.0,'S':0.0,'E':-0.25,'W':+0.25}
        y = {'N':0.25,'S':-0.25,'E':0.0,'W':0.0}
        dx = {'N':0.0,'S':0.0,'E':0.4,'W':-0.4}
        dy = {'N':-0.4,'S':0.4,'E':0.0,'W':0.0}
        plt.clf()
        ax = plt.gca()
        value = np.asarray(self.value).transpose()
        plt.imshow(value,vmin=0,vmax=1,cmap='coolwarm')
        for ix in range(self.nx+2):
            for iy in range(self.ny+2):
                dir = dir_list[self.opt_action[ix,iy]]
                if (self.maze.cell[ix,iy] == 0):
                    plt.arrow(ix+x[dir],iy+y[dir],dx[dir],dy[dir],width=0.05)
                if (self.maze.cell[ix,iy] == 1):
                    # Wall
                    rect = patches.Rectangle((ix-0.5,iy-0.5),1,1,facecolor='white',edgecolor='black',linewidth=2)
                    ax.add_patch(rect)
                if (self.maze.cell[ix,iy] > 1):
                    # Wall
                    rect = patches.Rectangle((ix-0.5,iy-0.5),1,1,facecolor='none',edgecolor='green',linewidth=1)
                    ax.add_patch(rect)
        plt.savefig('optimal_value.pdf',bbox_inches='tight')

    def show_value_function(self):
        '''Print out the optimal value function'''
        if (self.value is None):
            return
        for iy in range(self.ny+2):
            s = ''
            for ix in range(self.nx+2):
                if (self.maze.cell[ix,iy] != 1):
                    s += ('%7.2f ' % self.value[ix,iy])
                else:
                    s += ('%7s ' % '')
            print (s)
        print ()

    def show_policy(self):
        '''Print out the optimal policy'''
        if (self.opt_action is None):
            return
        dir_list = ('N','S','E','W')
        for iy in range(self.ny+2):
            s = ''
            for ix in range(self.nx+2):
                if (self.maze.cell[ix,iy] == 0):
                    s += dir_list[self.opt_action[ix,iy]]+'  '
                else:
                    s += '   '
            print (s)
        
##############################################################
# M A I N
##############################################################
if (__name__ == '__main__'):
    # Cost of running into a wall
    cost_wall = -0.2
    # Cost of moving
    cost_move = -0.025
    # Dicount factor
    gamma = 0.99
    small_maze = False
    if (small_maze):
        # Build a small 4x3 maze for testing
        maze = Maze(4,3,cost_move=cost_move,cost_wall=cost_wall)
        maze.add_reward(4,3,+1)
        maze.add_reward(4,2,-1)
        maze.add_wall(2,2)
    else:
        # Build a large 32x32 maze
        maze = Maze(32,32,cost_move=cost_move,cost_wall=cost_wall)
        maze.add_reward(15,3,+1)
        maze.add_reward(12,20,+0.8)
        maze.add_reward(30,20,+0.9)
        maze.add_reward(30,12,-1)
        maze.add_reward(30,2,+0.7)
        maze.add_reward(5,12,-1)
        for i in range(10):
            maze.add_wall(8+i,9)
        for j in range(20):
            maze.add_wall(25,5+j)
        for j in range(12):
            maze.add_wall(15,15+j)

    # Optimize maze and show results
    maze.show()
    optimizer = MazeOptimizer(maze)
    optimizer.value_function(gamma)
    optimizer.show_value_function()
    optimizer.show_policy()
    optimizer.plot()
    
