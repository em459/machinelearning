Some experiments with machine learning
======================================

MNIST
-----
Tensorflow implementation of softmax regression for MNIST dataset
See [Andrew Ng's lecture notes on supervised learning](http://cs229.stanford.edu/notes/cs229-notes1.pdf).

Support Vector Machines
-----------------------
Implementation of (non-linear) support vector machine for data classification. See [Andrew Ng's lecture notes on SVMs](http://cs229.stanford.edu/notes/cs229-notes3.pdf).