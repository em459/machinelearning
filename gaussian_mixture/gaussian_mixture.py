from __future__ import print_function
import sys
import numpy as np
import math
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
import subprocess
import ctypes
import time

def generate_data(specs,n_total):
    data_points = []
    labels = []
    for i,spec in enumerate(specs):
        mu,sigma,theta = spec
        for n in range(int(theta*n_total)):
            data_points.append(np.asarray(np.random.multivariate_normal(mu,sigma)))
            labels.append(i)
    return np.asarray(data_points),np.asarray(labels)
        
class GaussianMixtureModel(object):
    '''Gaussian mixture model for a fixed set of components

    :arg n_gaussian: number of Gaussians used to describe the data
    :arg use_c_library: use the optimised C-library instead of native
                        Python for the EM-step
    :arg plot: plot at every iteration
    '''
    def __init__(self,n_gaussian,use_c_library=True,plot=False):
        # Number of Gaussians
        self.n_gaussian = n_gaussian
        # Use compiled C-library?
        self._use_c_library = use_c_library
        # Plot at every iteration?
        self._plot=plot
        # Component probabilities
        self.phi = np.zeros(n_gaussian)
        # Component centres and covariances
        self.mu = np.zeros((n_gaussian,2))
        self.Sigma = np.zeros((n_gaussian,2,2))
        # Size of domain
        self.x_min = -10.0
        self.x_max = +10.0
        self.y_min = -10.0
        self.y_max = +10.0
        # Compile c code
        self._cc = 'gcc'
        self._cc_flags = '-fPIC'
        self._ar = 'gcc'
        self._ar_flags = '-shared'
        if (self._use_c_library):
            self._compile()

    def plot(self,data_points,labels,filename):
        '''Plot the current state and data and save to file

        :arg data_points: array with data points
        :arg labels: data labels
        :arg filename: name of file to write
        '''
        colors = ("red","blue","green","black")
        plt.clf()
        ax = plt.gca()
        ax.set_xlim(self.x_min,self.x_max)
        ax.set_ylim(self.y_min,self.y_max)
        X = data_points[:,0]
        Y = data_points[:,1]
        for i in range(self.n_gaussian):
            indices = np.where(labels==i)
            plt.plot(X[indices],Y[indices],
                     marker='o',
                     markersize='4',
                     linewidth=0,
                     markeredgewidth=0,
                     color=colors[i])
        x = np.arange(self.x_min, self.x_max, 0.01)
        y = np.arange(self.y_min, self.y_max, 0.01)
        X, Y = np.meshgrid(x, y)
        for j in range(self.n_gaussian):
            color = 'black'
            plt.plot((self.mu[j,0],),(self.mu[j,1],),
                     marker='o',
                     markersize='8',
                     markerfacecolor='white',
                     markeredgewidth=2,
                     markeredgecolor=color,
                    color=color
            )
            X_ = X - self.mu[j,0]
            Y_ = Y - self.mu[j,1]
            Sigma_inv = np.linalg.inv(self.Sigma[j,:,:])
            Z = 0.5*(Sigma_inv[0,0]*X_**2 + 2.*Sigma_inv[0,1]*X_*Y_ + Sigma_inv[1,1]*Y_**2)
            levels = (0,1,4)
            plt.contour(X, Y, Z, levels,
                        colors=('k',),
                        linewidths=(2,))
        plt.savefig(filename,bbox_inches='tight')

    def _compile(self):
        source = r'''
        #include <math.h>
        #include <stdlib.h>

        #define TWO_PI (2.0 * 3.14159265358979323846)

        /* == Evaluate Gaussian at a point ==
         * mu [in]    : centre of Gaussian
         * Sigma [in] : covariance of Gaussian
         * x [in]     : point at which to evaluate Gaussian
         */
        double p_gaussian(double* mu, double* Sigma, double* x) {
          // normalisation constant
          double detSigma = Sigma[0]*Sigma[3]-Sigma[1]*Sigma[2];
          double C_ = 1./sqrt(TWO_PI*detSigma);
          double x_ = x[0]-mu[0];
          double y_ = x[1]-mu[1];
          // (x-mu)^T.Sigma.(x-mu)
          double xT_Sigma_x = Sigma[3]*x_*x_
                            - (Sigma[1]+Sigma[2])*x_*y_
                            + Sigma[0]*y_*y_;
          return C_*exp(-0.5/detSigma*xT_Sigma_x);
        }

        #define MU_IDX(j,k) (2*(j)+(k)) 
        #define SIGMA_IDX(j,k) (4*(j)+(k))
        #define X_IDX(i,k) (2*(i)+(k))
        #define W_IDX(i,j) (n_gaussian*(i)+(j))

        /* == step of EM algorithm ==
         * n_gaussian [in]  : number of Gaussians
         * n_data [in]      : number of data points
         * data_points [in] : array of size (n_data) with data points
         * w [inout]        : temporary array of size (n_gaussian,n_data)
         *                    with weights of all points
         * phi [out]        : array of size (n_gaussian) with probabilities
         * mu [out]         : array of size (n_gaussian,2) with centres of 
         *                    normal distributions
         * Sigma [out]      : array of size (n_gaussian,2,2) with covariances 
         *                    of normal distributions
         */
          void step(int n_gaussian,
                    int n_data,
                    double* data_points,
                    double* w,
                    double* phi,
                    double* mu,
                    double *Sigma) {
          // *** E-step ***
          for (int i=0;i<n_data;++i) {
            double norm = 0.0;
            for (int j=0;j<n_gaussian;++j) {
              w[W_IDX(i,j)] *= p_gaussian(&mu[MU_IDX(j,0)],
                                          &Sigma[SIGMA_IDX(j,0)],
                                          &data_points[X_IDX(i,0)]);
              norm += w[W_IDX(i,j)];
            }
            for (int j=0;j<n_gaussian;++j) {
              w[W_IDX(i,j)] *= 1./norm;
            }
          }
          // *** M-step ***
          for (int j=0;j<n_gaussian;++j) {
            double sum_w = 0.0;
            double sum_wx[2] = {0.0, 0.0};
            double sum_wxx[4] = {0.0, 0.0, 0.0, 0.0}; 
            for (int i=0;i<n_data;++i) {
              sum_w += w[W_IDX(i,j)];
              sum_wx[0] += w[W_IDX(i,j)]*data_points[X_IDX(i,0)];
              sum_wx[1] += w[W_IDX(i,j)]*data_points[X_IDX(i,1)];
            }
            phi[j] = sum_w/n_data;
            mu[MU_IDX(j,0)] = sum_wx[0]/sum_w;
            mu[MU_IDX(j,1)] = sum_wx[1]/sum_w;
            for (int i=0;i<n_data;++i) {
              double x_ = data_points[X_IDX(i,0)]-mu[MU_IDX(j,0)];
              double y_ = data_points[X_IDX(i,1)]-mu[MU_IDX(j,1)];
              double w_ = w[W_IDX(i,j)];
              sum_wxx[0] += w_*x_*x_;
              sum_wxx[1] += w_*x_*y_;
              sum_wxx[2] += w_*y_*x_;
              sum_wxx[3] += w_*y_*y_;
            }
            Sigma[SIGMA_IDX(j,0)] = sum_wxx[0]/sum_w;
            Sigma[SIGMA_IDX(j,1)] = sum_wxx[1]/sum_w;
            Sigma[SIGMA_IDX(j,2)] = sum_wxx[2]/sum_w;
            Sigma[SIGMA_IDX(j,3)] = sum_wxx[3]/sum_w;
          }
        }

        /* === log-likelihood of data ===
         * n_gaussian [in]  : number of Gaussians
         * n_data [in]      : number of data points
         * data_points [in] : array of size (n_data) with data points
         * w [inout]        : temporary array of size (n_gaussian,n_data)
         *                    with weights of all points
         * phi [out]        : array of size (n_gaussian) with probabilities
         * mu [out]         : array of size (n_gaussian,2) with centres of 
         *                    normal distributions
         * Sigma [out]      : array of size (n_gaussian,2,2) with covariances 
         *                    of normal distributions
         */
        double log_likelihood(int n_gaussian,
                              int n_data,
                              double* data_points,
                              double* phi,
                              double* mu,
                              double *Sigma) {
          double log_ell = 0.0;
          for(int i=0;i<n_data;++i) {
            double p = 0.0;
            for(int j=0;j<n_gaussian;++j) {
              p += phi[j]*p_gaussian(&mu[MU_IDX(j,0)],
                                     &Sigma[SIGMA_IDX(j,0)],
                                     &data_points[X_IDX(i,0)]);
            }
            log_ell += log(p);
          }
          return log_ell;
        }
        '''
        lib_name= 'lib_em'
        with open(lib_name+'.c','w') as f:
            print (source,file=f)
        cmd_cc = [self._cc,
                  '-c',
                  self._cc_flags,
                  lib_name+'.c',
                  '-o',
                  lib_name+'.o']
        subprocess.check_output(cmd_cc)
        cmd_ar = [self._cc,
                  self._ar_flags,
                  lib_name+'.o',
                  '-o',
                  lib_name+'.so']
        subprocess.check_output(cmd_ar)
        self.lib_em = ctypes.cdll.LoadLibrary(lib_name+'.so')
        self.lib_em.log_likelihood.restype = ctypes.c_double

    def step(self,data_points,w):
        '''Carry out a single step of the EM algorithm for a given set 
        of data points
        
        :arg data_points: data points
        :arg w: Weight array where w_{j}^{(i)} is the probability that point i
                belongs to class j
        '''
        n_gaussian = len(self.phi)
        n_data = data_points.shape[0]
        double_p = ctypes.POINTER(ctypes.c_double)
        if (self._use_c_library):
            # Call EM-step from dynamically linked library
            self.lib_em.step(
                ctypes.c_int(n_gaussian),
                ctypes.c_int(n_data),
                data_points.ctypes.data_as(double_p),
                w.ctypes.data_as(double_p),
                self.phi.ctypes.data_as(double_p),
                self.mu.ctypes.data_as(double_p),
                self.Sigma.ctypes.data_as(double_p))
        else:
            w_tmp = np.zeros(self.n_gaussian)
            # === E-step ===
            for i in range(n_data):
                norm = 0.0
                for j in range(self.n_gaussian):
                    w_tmp[j] = w[i,j]*self.p_gaussian(self.mu[j,:],
                                                      self.Sigma[j,:,:],
                                                      data_points[i])
                    norm += w_tmp[j]
                w[i,:] = 1./norm*w_tmp[:]
            # === M-step ===
            for j in range(self.n_gaussian):
                sum_w = 0.0
                sum_wx = np.zeros(2)
                for i in range(n_data):
                    sum_w += w[i,j]
                    sum_wx[:] += w[i,j]*data_points[i]
                self.phi[j] = (1./n_data)*sum_w
                self.mu[j,:] = (1./sum_w)*sum_wx[:]
                sum_wxx = np.zeros((2,2))
                for i in range(n_data):
                    x_ = data_points[i]-self.mu[j,:]
                    sum_wxx[:,:] += w[i,j]*np.tensordot(x_,x_,axes=0)
                self.Sigma[j,:,:] = (1./sum_w)*sum_wxx[:,:]

    def p_gaussian(self,mu,Sigma,x):
        '''Evaluate value of Gaussian centred at mu with covariance Sigma at
           the x 

        :arg mu: centre of Gaussian
        :arg Sigma: covariance of Gaussian
        :arg x: point at which to evaluate Gaussian
        '''
        C = 1./math.sqrt(2.*math.pi*np.linalg.det(Sigma))
        xT_Sigma_x = np.dot(np.dot(np.linalg.inv(Sigma),x-mu),x-mu)
        return C*math.exp(-0.5*xT_Sigma_x)

    def log_likelihood(self,data_points,
                       phi=None,
                       mu=None,
                       Sigma=None):
        '''Evalate log-likelihood for current distribution of Gaussians for
        data vector data_points

        :arg data_points: data vector
        :arg phi: probabilities (use self.phi if None) 
        :arg mu: centres of Gaussians (use self.mu if None) 
        :arg Sigma: covariance matrices of Gaussians (use self.Sigma if None) 
        '''
        if (phi is None):
            phi_ = self.phi
        else:
            phi_ = phi
        if (mu is None):
            mu_ = self.mu
        else:
            mu_ = mu
        if (Sigma is None):
            Sigma_ = self.Sigma
        else:
            Sigma_ = Sigma
        n_data = len(data_points)
        if (self._use_c_library):
            double_p = ctypes.POINTER(ctypes.c_double)
            log_ell = self.lib_em.log_likelihood(
                ctypes.c_int(n_gaussian),
                ctypes.c_int(n_data),
                data_points.ctypes.data_as(double_p),
                phi_.ctypes.data_as(double_p),
                mu_.ctypes.data_as(double_p),
                Sigma_.ctypes.data_as(double_p))
        else:            
            log_ell = 0.0
            for i in range(n_data):
                p = 0.0
                for j in range(self.n_gaussian):
                    p += phi_[j]*self.p_gaussian(mu_[j,:],
                                                     Sigma_[j,:,:],
                                                     data_points[i])
                log_ell += math.log(p)
        return log_ell

    def iterate(self,data_points,
                labels,
                maxiter=64,
                tolerance=1.E-3,
                phi=None,
                mu=None,
                Sigma=None):
        '''Maximise likelihood using the EM algorithm
        
        :arg data_points: list with data points
        :arg labels: list of labels (only used for plotting)
        :arg maxiter: maximal number of iterations
        :arg tolerance: abort when relative change in tolerance is below this
                        value
        '''
        # number of data points
        m = len(data_points)
        x = data_points
        x_min = np.min(data_points[:,0])
        x_max = np.max(data_points[:,0])
        y_min = np.min(data_points[:,1])
        y_max = np.max(data_points[:,1])
        dx = x_max-x_min
        dy = y_max-y_min
        self.x_min=x_min-0.1*dx
        self.x_max=x_max+0.1*dx
        self.y_min=y_min-0.1*dy
        self.y_max=y_max+0.1*dy
        # Weights
        w = np.zeros((m,self.n_gaussian))
        if ((phi is None) or (mu is None) or (Sigma is None)):
            # Randomly assign labels
            w[:,:] = 1./self.n_gaussian
            # Randomly pick location of Gaussians
            for j in range(n_gaussian):
                self.mu[j,0] = np.random.uniform(self.x_min,self.x_max)
                self.mu[j,1] = np.random.uniform(self.y_min,self.y_max)
                dx = 0.5*(self.x_max-self.x_min)
                dy = 0.5*(self.y_max-self.y_min)
                self.Sigma[j,:,:] = np.asarray([[dx,0.0],[0.0,dy]])

            self.phi[:] = 1./self.n_gaussian
        else:
            self.phi[:] = phi[:]
            self.mu[:,:] = mu[:,:]
            self.Sigma[:,:,:] = Sigma[:,:,:]
            for j in range(n_gaussian):
                w[:,j] = 1.0
        print ('=== EM iteration ===')
        print ('iter     log-likelihood')
        log_ell_old = self.log_likelihood(data_points)
        print ('  initial : %8.2f' % log_ell_old)
        if (self._plot):
            filename = 'output_'+('%03d' % (0))+'.png'
            self.plot(data_points, labels, filename)
        start_time = time.time()
        for k in range(maxiter):
            self.step(data_points,w)
            log_ell = self.log_likelihood(data_points)
            rho = abs((log_ell-log_ell_old)/log_ell)
            print (' %4d  : %8.2f    %8.2e' % ((k+1),log_ell,rho))
            if (rho < tolerance):
                break
            log_ell_old = log_ell
            if (self._plot):
                filename = 'output_'+('%03d' % (k+1))+'.png'
                self.plot(data_points, labels, filename)
        finish_time = time.time()
        elapsed_time = finish_time - start_time
        print ('number of iterations = %4d' % (k+1))
        print ('elapsed time         = %8.4f s' % elapsed_time)
        print ('time/iter            = %8.4f ms' % (1000.*elapsed_time/(k+1)))

def convert_specs(specs):
    '''Convert specifications to numpy arrays with probabilities, centre and
    covariances of Gaussians
    
    :arg specs: List of specifications. Each entry is of the form
                mu, Sigma, phi
    '''
    n_gaussian = len(specs)
    phi = np.zeros(n_gaussian)
    mu = np.zeros((n_gaussian,2))
    Sigma = np.zeros((n_gaussian,2,2))
    for j, s in enumerate(specs):
        mu[j,:] = s[0]
        Sigma[j,:,:] = s[1]
        phi[j] = s[2]
    return phi, mu, Sigma
    
if (__name__ == "__main__"):
    np.random.seed(1675147)
    n_total = 5120
    rho = 1.0
    specs = ( ((2.0,0.0),((rho*3.0,rho*0.5),(rho*0.5,rho*3.0)),0.2),
              ((-2.0,-2.0),((rho*1.5,rho*0.1),(rho*0.1,rho*1.5)),0.35),
              ((-2.0,4.5),((rho*1.4,rho*0.1),(rho*0.1,rho*1.2)),0.3),
              ((3.0,5.0),((rho*0.6,rho*0.1),(rho*0.1,rho*0.4)),0.15)
    )

    phi_exact, mu_exact, Sigma_exact = convert_specs(specs)
    synthetic_data = False
    if (synthetic_data):
        data_points, labels = generate_data(specs,n_total)
        n_gaussian = len(specs)
    else:
        n_gaussian = 2
        data_points = []
        with open('old_faithful.txt') as f:
            for line in f.readlines():
                tmp = line.split()
                data_points.append((float(tmp[1]),float(tmp[2])))
        data_points = np.asarray(data_points)
        labels = np.zeros(len(data_points))
    model = GaussianMixtureModel(n_gaussian,
                                 use_c_library=True,
                                 plot=False)
    start_time = time.time()
    model.iterate(data_points,labels,
                  maxiter=64,
                  tolerance=1.E-6)
    log_ell_exact = model.log_likelihood(data_points,
                                         phi_exact,
                                         mu_exact,
                                         Sigma_exact)
    if (synthetic_data):
        print ('log-likelihood of exact solution = ',log_ell_exact)
    model.plot(data_points, labels, 'output_final.pdf')
    
